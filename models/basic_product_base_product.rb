class BasicProductBaseProduct < ActiveRecord::Base
  self.table_name = 'basic_products_base_products'
  attr_accessible :product_unique_id, :base_product_unique_id, :match_type
end