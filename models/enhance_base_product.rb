class EnhanceBaseProduct

  def self.create_base_product
  
    ## Initialize & Load
    baseprods = {}; bp_ids = []; exist_bps = []; collect_bps = []
    BaseProduct.find(:all).each {|bp| baseprods[bp.name] = bp; bp_ids.push(bp.id)}
    inprods = InputBaseProduct.find_by_sql("select ttr, icrt, name, model, brand, category_id from input_base_products group by ttr, icrt, name, model, brand, category_id")
  
    ## Reset BaseProducts 'lastrun' column
    BaseProduct.connection.execute("update base_products set lastrun=null")
  
    ## Find Existing & New BaseProducts
    inprods.each do |ip|
      begin
        unless (bp=baseprods[ip.name]).nil?
          exist_bps.push(bp.id)
          next
        end
        next unless valid_model_name?(ip.model)
        name = ip.name

        uniqueid = Util.generate_base_product_unique_id(name, ip.category_id)
        collect_bps << (" select '#{ip.ttr}', '#{ip.icrt && ip.icrt.gsub("'","''")}', '#{ip.brand.gsub("'","''")}','#{ip.model.gsub("'","''")}',#{ip.category_id},'#{name.gsub("'","''")}','#{uniqueid}', '#{Date.today.to_s}', '#{Date.today.to_s}', 'new' ")
      rescue => e
        puts e.message
      end
    end
    
    ## Insert New BaseProducts
    unless collect_bps.blank?
      0.step(collect_bps.length, 5000) do |i|
        local_collect_bps = collect_bps[i,5000]
          select_query = local_collect_bps.join(" union all ")
          BaseProduct.connection.execute("insert into base_products(ttr, icrt, brand, model, category_id, name, unique_id, created_at,
            updated_at, lastrun) #{select_query}")
      end
    end

    ## delete from baseproducts which are not found in current run
    delete_bps = bp_ids-exist_bps
    0.step(delete_bps.length, 1000) do |x|
      BaseProduct.delete(delete_bps[x..x+1000].compact)
    end

    ## Update InputBaseProducts
    InputBaseProduct.connection.execute("update input_base_products set input_base_products.base_product_id=base_products.id from input_base_products, base_products where input_base_products.name=base_products.name")

  end

  def self.remove_base_product_not_used
    BaseProduct.connection.execute("delete from basic_products_base_products where base_product_unique_id not in (select unique_id from base_products)")
  end

  def self.update_categories
  base_product_list = BaseProduct.find(:all, :joins=>[:input_base_products], :conditions=>["base_products.category_id =-1 and input_base_products.category_id >0"],
                                :select=>["base_products.id bp_id, base_products.category_id bp_category_id, input_base_products.base_product_id ibp_bp_id, input_base_products.category_id ibp_category_id "])

    base_product_list.each do |bpl|
      BaseProduct.update_all("category_id = #{bpl.ibp_category_id}", "id=#{bpl.bp_id} and category_id =-1")
    end

  end

  def self.valid_model_name?(model)
    return true
    begin
      model.unpack("U*")
      true
    rescue Exception => e
      false
    end
  end


  def self.find_duplicate_input_base_products
    total = 0; total_prods = 0; total_approx = 0
    exact_str = ''; approx_str = ''; total_str = "";
    WorkitProductFamily::COUNTRY_NAMES.each do |country|
      Category.find(:all).each do |category|
        duplicates = []; ibp_split = []; ibp_group = {}; approx_duplicates = []
        con_cat="\n<b>===#{category.name}====#{country}====</b>"; puts con_cat; exact_str+=con_cat; approx_str+=con_cat
        ibp = InputBaseProduct.find_all_by_country_and_category_id(country, category.id)
        ibp_split = ibp.map {|e| name=e.name.mb_chars.normalize(:kd).gsub(/[^\x00-\x7F]/n,'').to_s.downcase; [e, name.gsub(/-/,' ').split(' '), name.gsub(/-|\s/, '').length]}
        ibp_split.each {|e| ibp_group[e[2]]||=[]; ibp_group[e[2]].push(e)}
        ibp_group.each do |k,v|
          v.each do |e1|
            v.each do |e2|
              next if e1[0]==e2[0]
              c=(e1[1]-e2[1]).join('')
              d=(e2[1]-e1[1]).join('')
              if c==d
                loc = nil
                duplicates.each_with_index {|dg,i| loc = i if (dg.include?(e1[0]) || dg.include?(e2[0])) }
                loc.nil? ? duplicates.push([e1[0], e2[0]]) : duplicates[loc].push(e1[0], e2[0])
              end
            end
          end
        end
        ibp_split.each do |e1|
          ibp_split.each do |e2|
            next if e1[0]==e2[0]
            c=(e1[1]-e2[1]).join('')
            d=(e2[1]-e1[1]).join('')
            if ((c.empty? && !d.empty?) || (d.empty? && !c.empty?))
              entry = [e1, e2].sort {|x,y| x[0].name <=> y[0].name}
              approx_duplicates.push(entry) unless approx_duplicates.include?(entry)
            end
          end
        end
        exact_temp_str = ""; approx_temp_str = ""
        duplicates = duplicates.map {|dg| dg.uniq}
        duplicates.each {|dg| exact_temp_str+="\n"; dg.each {|ibp| exact_temp_str+=ibp.name+"\n"}}; exact_str += exact_temp_str
        approx_duplicates.each {|e| approx_temp_str+="\n"+"#{e[0][0].name}\n"+"#{e[1][0].name}\n"}; approx_str += approx_temp_str
        total_str += "\n<b>===Exact dupl:#{category.name}====#{country}====</b>"+exact_temp_str+"\n"
        total_str += "\n<b>===Approx dupl:#{category.name}====#{country}====</b>"+approx_temp_str+"\n"
        total += duplicates.length
        total_approx += approx_duplicates.length
        duplicates.each {|dg| total_prods += dg.length}
      end
    end

    count_str  = "Total Exact Duplicate Groups : #{total}\nTotal Exact Duplicate Products : #{total_prods}\n"
    exact_str  = count_str+exact_str; total_str=count_str+total_str
    count_str  = "Total Approx Duplicate Groups : #{total_approx}\n"
    approx_str = count_str+approx_str; total_str=count_str+total_str
    [exact_str, approx_str, total_str]
  end

end
