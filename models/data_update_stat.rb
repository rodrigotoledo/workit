class DataUpdateStat < ActiveRecord::Base
  serialize :content

  attr_accessible :starttime, :runtime, :downloadtime, :closetime, :productscount, :created_at, :categorycount, :sitecount


  def self.create_last
    DataUpdateStat.create(
      starttime: Time.now
    )
  end

  def self.end_last
    last_date_of_crawler  = DataUpdateStat.last
    return false unless last_date_of_crawler
    # begin_of_yesterday    = Time.parse((Time.now - 1.day).strftime("%Y-%m-%d 00:00:00"))
    # if !last_date_of_crawler || begin_of_yesterday > last_date_of_crawler.created_at
      ended_at = Time.now
      time_spent = (ended_at.to_i - last_date_of_crawler.created_at.to_i)
      last_date_of_crawler.attributes = {
        runtime: time_spent,
        downloadtime: time_spent,
        closetime: ended_at,
        productscount: Product.joins(:site).where(sites: { source: 'workit' }).count ,
        categorycount: SiteCategory.joins("INNER JOIN sites ON site_categories.site_id = sites.id INNER JOIN products ON site_categories.id = products.site_category_id").where("sites.source = 'workit'" ).count,
        sitecount: Site.where("id IN (select site_id from products) and source = 'workit'").count
      }
      last_date_of_crawler.save
    # end
  end
end