class Product < ActiveRecord::Base

  attr_accessible :name, :price, :url, :site_id, :site_category_id, :raw_price, :accessory,
  :raw_name, :in_stock, :unique_id, :active, :last_found, :country, :trovaprezzi, :lastrun
  
  belongs_to :site_category
  belongs_to :site, :counter_cache => true
  belongs_to :all_product

  before_create :remove_attributes
  
  has_and_belongs_to_many :basic_base_products,
    :join_table => 'basic_products_base_products',
    :class_name => 'BaseProduct',
    :foreign_key => 'product_unique_id',
    :association_foreign_key => 'base_product_unique_id',
    :finder_sql => "SELECT base_products.* FROM base_products INNER JOIN basic_products_base_products ON base_products.unique_id = basic_products_base_products.base_product_unique_id WHERE (basic_products_base_products.product_unique_id = '" + '#{unique_id}' + "' )",
    :delete_sql => "delete from basic_products_base_products where product_unique_id = '" + '#{unique_id}' + "'",
    :insert_sql => "insert into basic_products_base_products(base_product_unique_id, product_unique_id) values('" + '#{record.unique_id}' + "','" + '#{unique_id}' + "')"

  has_many :all_product_prices, :foreign_key => 'unique_id', :primary_key => 'unique_id'
  # delegate :name, :to => :site, :prefix => true, :allow_nil => true


  def remove_attributes
    @attributes.delete('Numerical_price')
    @attributes.delete('currency')
    true
  end
end