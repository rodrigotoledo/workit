class Site < ActiveRecord::Base
  attr_accessible :name, :css_selector, :country, :disabled, :null_urls, :pretty_name, :source, :products_count,
  :unique_id, :evaluated, :excluded
  has_many :site_categories
end