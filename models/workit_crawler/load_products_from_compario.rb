module WorkitCrawler
  class LoadProductsFromCompario

    def initialize
      
    end

    def create
      clean_input_base_products
      create_input_base_products
      clean_null_input_base_products
      EnhanceBaseProduct.create_base_product
      WorkitBplList.rebuild
    end

    def create_input_base_products
      input_base_products_from_compario = []
      countries.each do |country|
        begin
          input_base_products_from_compario = input_base_products_from_compario(country)
          WorkitCrawler.puts_action("Products collected from compario of #{country}: #{input_base_products_from_compario.size}")

          begin
            ActiveRecord::Base.establish_connection(@@connection_info[ENV['RACK_ENV']])
            InputBaseProduct.create!(input_base_products_from_compario)
            WorkitCrawler.puts_action("#{input_base_products_from_compario.size} products migrated from #{country}")
          rescue => create_exception
            raise create_exception.message
          end
        rescue => e
          WorkitCrawler.puts_action("Error on migrate compario products from #{country}")
          WorkitCrawler.puts_action(e.message)
        end
      end

    end


    def input_base_products_from_compario(country)
      _categories_to_id           = categories_to_id(country)
      _categories_names_to_sql_in = categories_names_to_sql_in(country)

      compario            = LoadProductsFromCompario.compario_connection(country)
      sql_on_compario     = "select 
          [Total score] ttr, [ICRT Code] icrt, brand, model, country = #{country_map}, encodex_id, provided_activation_status = #{status_map}  ,
          category_id = CASE #{_categories_to_id} ELSE -1 END , Brand+' '+Model as name, tested, ComparioId, \"Encodex Average Price\" EncodexPrice,photo,
          ProdNom, ProductName, best_buy =case bestbuy when 'False' then 0 when null then 0  when '' then 0 else 1 end ,
          best_of_the_test = case  bestofthetest when 'False' then 0 when null then 0  when '' then 0  else 1 end ,
          profitable_choice = case cheapbuy when 'False' then 0 when null then 0  when '' then 0 else 1 end 
        FROM OnlinePriceReport
        WHERE familyName in (#{_categories_names_to_sql_in})"
      input_base_products = compario.connection.select_all(sql_on_compario)

      # WorkitCrawler.puts_action(sql_on_compario)
      compario.disconnect!
      input_base_products
    end
    

    def categories_by_country(country)
      @categories ||= {}
      @categories[country] ||= Category.where(id: WorkitProductFamily.where(country: country).all.map{|workit_product_family| workit_product_family.category_id})
    end

    def categories_names_to_sql_in(country)
      categories_by_country(country).collect{|category| "'#{category.name}'" }.join(',')
    end

    def categories_to_id(country)
      categories = categories_by_country(country).collect{|category| {name: category.name, id: category.id}}
      categories.collect{|category| "WHEN FamilyName = '#{category[:name]}' then #{category[:id]}"}.join(' ')
    end

    def countries
      WorkitProductFamily.select("DISTINCT country").all.collect{|t| t.country_short_name}
    end

    def country_map
      "CASE WHEN Language = 'BE_NL' THEN 'belgium_nl' WHEN Language = 'BE_FR' THEN 'belgium_fr' WHEN Language = 'BR_BR' THEN 'brazil' WHEN Language = 'ES_ES' THEN 'spain' WHEN Language = 'IT_IT' THEN 'italy' WHEN Language = 'IT' THEN 'italy' WHEN Language = 'PT_PT' THEN 'portugal' ELSE 'undefined' END"
    end

    def status_map
      "CASE WHEN Status = '1' THEN 1 WHEN Status = 'True' THEN 1 ELSE 0 END"
    end

    def self.database_definitions
      @database_definitions ||= YAML::load_file(File.join(File.dirname(File.expand_path(__FILE__)),'..', '../config', 'compario_database.yml'))
    end

    def self.compario_connection(country)
      connection_settings = self.database_definitions[ENV['RACK_ENV']]["compario_#{country}"]
      return false unless connection_settings
      begin
        ActiveRecord::Base.establish_connection(connection_settings)
      rescue
        raise 'Invalid connection information'
      end
    end
    

    def clean_input_base_products
      category_ids = WorkitProductFamily.select("DISTINCT category_id").all.collect{|t| t.category_id}.join(',')
      InputBaseProduct.connection.execute("DELETE FROM input_base_products WHERE category_id IN(#{category_ids})")
    end

    def clean_null_input_base_products
      InputBaseProduct.connection.execute("DELETE FROM input_base_products WHERE brand IS NULL and model IS NULL")
    end
  end
end
