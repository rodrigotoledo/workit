require 'csv'
module WorkitCrawler
  class WorkitSender
    FTP_PUT_FILES_ADDRESS       = 'in.w2p.workit.fr'
    FTP_USER                    = 'euroconsumer'
    FTP_PASSWORD                = 'w6xi6VD3'
    PATH_TO_WORKIT_FILES        = File.join('tmp','workit_files','generated')
    PATH_TO_WORKIT_FILES_ON_FTP = File.join(PATH_TO_WORKIT_FILES,'on_ftp')

    def initialize(export_type = 'csv')
      prepare_directory
      @export_type = export_type
    end
    
    def generate_and_send
      WorkitCrawler.puts_action("Send file to FTP started at #{Time.now}")
      if self.csv?
        generate_csv
      else
        generate_xml
      end
      send_to_ftp
      WorkitCrawler.puts_action("Send file to FTP ended at #{Time.now}")
    end

    protected

    def generate_csv
      final_file = "BPL_#{Date.today.strftime('%Y%m%d')}.CSV"
      WorkitCrawler.puts_action("Generating file #{final_file}")

      CSV.open(File.join(PATH_TO_WORKIT_FILES,final_file), "wb", col_sep: ';') do |csv|
        csv << ['Family', 'FamilyName', 'Country', 'Product', 'Model', 'Brand', 'Encodex Average Price', 'ICRT', 'SKU']

        workit_product_families = WorkitProductFamily.all
        workit_product_families.each do |workit_product_family|
          workit_product_family.workit_bpl_lists.each do |workit_bpl_list|
            csv << [
              workit_product_family.category.id,
              workit_product_family.category.name,
              workit_product_family.country,
              workit_bpl_list.name,
              workit_bpl_list.model,
              workit_bpl_list.brand,
              0.00,
              workit_bpl_list.icrt,
              workit_bpl_list.base_product_id
            ]
          end
        end
      end
    end

    def generate_xml
      workit_product_families = WorkitProductFamily.all
      builder                 = Nokogiri::XML::Builder.new do |xml|
      xml.euroconsumer {
        xml.categories {
          workit_product_families.each do |workit_product_family|
            xml.category{
              xml.id_ workit_product_family.category.id
              xml.name workit_product_family.category.name
            }
          end
        }
        xml.bpl{
          workit_product_families.each do |workit_product_family|
            workit_product_family.workit_bpl_lists.each do |workit_bpl_list|
              xml.product{
                xml.id_ workit_bpl_list.base_product_id
                xml.category_id workit_bpl_list.category_id
                xml.brand workit_bpl_list.brand
                xml.model workit_bpl_list.model
                xml.name workit_bpl_list.name
                xml.icrt workit_bpl_list.icrt
              }
            end
          end
        }
      }
      end

      final_file = "BPL_#{Date.today.strftime('%Y%m%d')}.XML"
      WorkitCrawler.puts_action("Generating file #{final_file}")
      File.open(File.join(PATH_TO_WORKIT_FILES,final_file), 'w') {|f| f.write(builder.to_xml) }
      
    end

    def send_to_ftp
      current_files = generated_files
      begin
        ftp         = Net::FTP.new(FTP_PUT_FILES_ADDRESS, FTP_USER, FTP_PASSWORD)
        ftp.passive = true
        WorkitCrawler.puts_action("Connected on ftp")
        current_files.each do |file|
          WorkitCrawler.puts_action("Sending ftp file #{File.basename(file)}")
          ftp.puttextfile(file)
          FileUtils.mv(file,PATH_TO_WORKIT_FILES_ON_FTP)
        end
      rescue => e
        WorkitCrawler.puts_action("error on ftp")
        WorkitCrawler.puts_action(e.message)
      end
    end

    def generated_files
      extension   = self.csv? ? '.CSV' : '.XML'
      xml_pattern = File.join(PATH_TO_WORKIT_FILES,"*#{extension}")
      Dir.glob(xml_pattern) || []
    end

    def csv?
      @export_type == 'csv'
    end

    def prepare_directory
      FileUtils.mkdir_p(PATH_TO_WORKIT_FILES, mode: 0777) unless Dir.exists?(PATH_TO_WORKIT_FILES)
      FileUtils.mkdir_p(PATH_TO_WORKIT_FILES_ON_FTP, mode: 0777) unless Dir.exists?(PATH_TO_WORKIT_FILES_ON_FTP)
    end
    
  end
end