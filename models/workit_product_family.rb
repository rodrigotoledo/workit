class WorkitProductFamily < ActiveRecord::Base
  CATEGORY_NAMES = ['Dishwashers PDH', 'Washing Machines PDH', 'Mobile Phones PDH', "Panel TVs PDH",
    'Vacuum Cleaners PDH', 'Printers PDH', 'GPS PDH', 'Digital cameras PDH', 'Camcorders PDH', 'Tablet PC PDH']
  COUNTRY_NAMES = ['italy','belgium_nl', 'belgium_fr', 'portugal', 'spain']
  belongs_to :category
  attr_accessible :category_id, :base_product, :country
  has_many :workit_bpl_lists

  def country_short_name
    case self.country
      when 'italy'
        'IT_IT'
      when 'belgium_fr'
        'BE_FR'
      when 'belgium_nl'
        'BE_NL'
      when 'brazil'
        'BR_BR'
      when 'portugal'
        'PT_PT'
      when 'spain'
        'ES_ES'
      else
        'IT_IT'
    end
  end

  def self.rebuild_by_countries_and_categories(countries = nil, categories = nil)
    categories ||= CATEGORY_NAMES
    countries  ||= COUNTRY_NAMES
    return if categories.blank?
    categories = Category.where(name: categories)
    countries.each do |country|
      WorkitProductFamily.where(country: country).delete_all
      categories.each do |category|
        next if WorkitProductFamily.where(country: country, category_id: category.id).first
        WorkitProductFamily.create(country: country, category_id: category.id)
      end
    end
  end
end