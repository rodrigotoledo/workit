class InputBaseProduct < ActiveRecord::Base
  attr_accessible :brand, :model, :country, :encodex_id, :provided_activation_status, :category_id,
  :base_product_id, :first_activity_period, :ean, :deleted_at, :name, :tested,
  :EncodexPrice, :ComparioId, :photo, :ProdNom, :ttr, :icrt, :ProductName, 
  :best_buy, :best_of_the_test, :profitable_choice

  belongs_to :category
  belongs_to :base_product
end