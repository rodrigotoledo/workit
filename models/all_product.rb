class AllProduct < ActiveRecord::Base
  belongs_to :site
  belongs_to :site_category
  attr_accessible :name, :price, :url, :site_id, :site_category_id, :raw_price, :accessory,
  :raw_name, :in_stock, :unique_id, :active, :last_found, :country, :trovaprezzi, :lastrun

  before_save :create_clean_url
  before_create :create_unique_id
  after_save :create_all_product_price

  def generate_unique_id
    if self.url.blank? or self.url.match(/stats.shoppydoo.com/).nil?
      Util.generate_unique_id(self.url, self.name, self.site_category_id, self.site_id)
    else
      Util.generate_unique_id(self.url.gsub(/\&cr=[^=]*$/,""), nil, nil, nil)
    end
  end

  def create_unique_id
    self.unique_id = generate_unique_id
    true
  end
  
  def create_clean_url
    self.url = Util.remove_tmp_params_from_url(self.url) unless self.url.blank?
    true
  end

  def create_all_product_price
    crawler       = DataUpdateStat.last.id
    history_price = AllProductPrice.where(:unique_id => self.unique_id, :price_date => Date.today.to_s).first


    all_product_price_attributes = {
      :price_date           => Date.today.to_s,
      :price                => self.price_in_currency, 
      :price_currency       => self.price.split(" ").first, 
      :data_update_stat_id  => crawler
    }

    if self.in_stock.to_i == 0
      all_product_price_attributes[:indicator_type_used] = 'n'
      all_product_price_attributes[:reason_rejected]     = 'Product Out of Stock'
    end

    unless history_price
      all_product_price_attributes[:unique_id] = self.unique_id
      AllProductPrice.create!(all_product_price_attributes)
    else
      history_price.update_attributes(all_product_price_attributes)
    end
    true
  end

  def price_in_currency
    self.price.to_s.gsub("R$",'').gsub('&euro;','').strip.to_f.round(2)
  end
end