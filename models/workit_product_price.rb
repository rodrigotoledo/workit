class WorkitProductPrice < ActiveRecord::Base
  scope :waiting_processing, where(status: 'waiting-processing')
  belongs_to :workit_bpl_list
  has_one :workit_product_family, through: :workit_bpl_list
  
  attr_accessible :workit_bpl_list_id, :base_product_id, :category_id, :icrt_code, :site_name, :site_id, :brand, :url,
  :title, :price, :variation, :availability, :delivery, :image_url, :modified_at, :delivery_price, :status, :import_errors, :processing_errors

  def country
    self.workit_product_family.country
  end

  def processed!
    self.update_attributes({
      status: 'successfully-processed',
      processing_errors: nil,
    })
  end

  def processed_with_error(message)
     self.update_attributes({
      status: 'error-on-processing',
      processing_errors: message,
    })
  end

  def self.save_with_error_on_importing(message, attributes)
    import_errors = []
    import_errors << "Error message:"
    import_errors << message
    import_errors << "--------------------------------------------------------"
    import_errors << 'Attributes:'
    import_errors << attributes.inspect
     self.new({
      status: 'error-on-importing',
      import_errors: import_errors.join("\n"),
    }).save(validate: false)
  end
end