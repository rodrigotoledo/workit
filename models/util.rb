class Util
  def self.remove_tmp_params_from_url(url)
    url1 = url
    return url if url.blank?
    murl = URI.parse(URI.escape(url))
    i = murl.path.index(';jsessionid=')
    unless i.nil?
      murl.path = murl.path[0..i-1]
      url1 = murl.to_s
    end
    unless url1.index(/\?osCsid=/).nil?
      url1.gsub!(/\?osCsid=[^\&]*\&/,'?')
    end
    if !url.index(/\/dp\//).nil? && !url.index(/\/ref=sr_/).nil?
      index = url.index(/\/ref=sr_/)
      url1 = url[0..index]
    end
    url1.gsub!(/tipovitrine=vitrine_media[^&]+/i,'')
    url1.gsub!(/\&{2,}/,"&")
    url1
  rescue => e
    url
  end

  def self.generate_unique_id(url, name, site_category_id, site_id)
    url = Util.remove_tmp_params_from_url(url) unless url.blank?

    Digest::MD5.hexdigest(url || (name + " " + site_category_id.to_s + " " + site_id.to_s))
  end

  def self.generate_base_product_unique_id(name, category_id)
    Digest::MD5.hexdigest(name + " " + category_id.to_s)
  end

  def self.generate_site_and_category_unique_id(id, name)
    id = Digest::MD5.hexdigest(id.to_s + ' ' + name)
    "#{id[0..7]}-#{id[8..11]}-#{id[12..15]}-#{id[16..19]}-#{id[20..-1]}"
  end
end
