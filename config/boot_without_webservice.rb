ENV["RACK_ENV"] ||= "development"

require 'bundler'
Bundler.setup

Bundler.require(:default, ENV["RACK_ENV"].to_sym)

require File.expand_path(File.join(File.dirname(__FILE__), "estabilish_connection"))

Dir["./models/**/*.rb"].each { |f| require f }