require 'spec_helper'

describe WorkitCrawler::WorkitLoad do
  before(:each) do
    WorkitCrawler.stub(:puts_action).and_return(true)
  end

  describe 'Constants' do
    it 'defined FTP_ADDRESS' do
      WorkitCrawler::WorkitLoad::FTP_GET_FILES_ADDRESS.should eql('out.w2p.workit.fr')
    end

    it 'defined FTP_USER' do
      WorkitCrawler::WorkitLoad::FTP_USER.should eql('euroconsumer')
    end

    it 'defined FTP_PASSWORD' do
      WorkitCrawler::WorkitLoad::FTP_PASSWORD.should eql('w6xi6VD3')
    end

    it 'defined PATH_TO_WORKIT_FILES' do
      WorkitCrawler::WorkitLoad::PATH_TO_WORKIT_FILES.should eql('tmp/workit_files')
    end

    it 'defined PATH_TO_IMPORTED_WORKIT_FILES' do
      WorkitCrawler::WorkitLoad::PATH_TO_IMPORTED_WORKIT_FILES.should eql('tmp/workit_files/imported')
    end
  end

  context 'Sync files to ftp' do
    context 'error on ftp' do
      it 'connection fail' do
        Net::FTP.should_receive(:new).and_raise('error on connect')
        WorkitCrawler.should_receive(:puts_action).exactly(2).times
        workit_load = WorkitCrawler::WorkitLoad.new
        workit_load.sync_ftp_files
      end
    end

    context 'sync stub files' do
      it 'files syncronized' do
        stub_const("WorkitCrawler::WorkitLoad::PATH_TO_WORKIT_FILES", 'spec/workit_xml_files')
        ftp = double('Ftp server').as_null_object
        Net::FTP.should_receive(:new).and_return(ftp)
        workit_load = WorkitCrawler::WorkitLoad.new
        workit_load.files_to_process.should have(1).elements
      end
    end
  end

  # context 'crawler running' do
  #   it 'not save file because crawler is running' do
  #     CrawlerStatus.stub(:running_now?).and_return(true)
  #     WorkitCrawler.should_receive(:puts_action).with('Crawler running now').at_least(1).times
  #     workit_load = WorkitCrawler::WorkitLoad.new
  #     workit_load.crawler_running?.should
  #   end
  # end

  # context 'crawler not running' do

  #   it 'save file because crawler isnt running' do
  #     CrawlerStatus.stub(:running_now?).and_return(false)
  #     WorkitCrawler.should_not_receive(:puts_action).with('Crawler running now')
  #     workit_load = WorkitCrawler::WorkitLoad.new
  #     workit_load.crawler_running?.should be_false
  #   end
  # end

  context 'No files to be imported' do
    before(:each) do
      WorkitCrawler::WorkitLoad.any_instance.stub(:files_to_process).and_return([])
      @workit_load = WorkitCrawler::WorkitLoad.new
    end

    it '0 files to be imported' do
      @workit_load.files_to_process.should have(0).elements
    end

    it 'message with no files to be imported' do
      WorkitCrawler.should_receive(:puts_action).exactly(3).times
      @workit_load.save_files_on_database
    end

    it 'crawler status up and down' do
      @workit_load.save_files_on_database
    end
  end

  context 'files to be imported' do

    it 'import 1 file' do
      WorkitCrawler::WorkitLoad.any_instance.stub(:files_to_process).and_return(['file1.xml'])
      workit_load = WorkitCrawler::WorkitLoad.new
      workit_load.stub(:save_file).and_return(true)
      workit_load.should_receive(:save_file).exactly(1).times
      workit_load.save_files_on_database
    end
  end
end