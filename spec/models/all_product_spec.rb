require "spec_helper"

describe AllProduct do
  before(:each) do
    FactoryGirl.create(:data_update_stat)
  end
  context "callbacks on create" do
    it '#create_clean_url' do
      all_product = FactoryGirl.create(:all_product, url: 'http://www.amazon.fr/gp/browse/ref=sr_&/dp/=1338582372')
      all_product.url.should eql('http://www.amazon.fr/gp/browse/')
    end
  end
end