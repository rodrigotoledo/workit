require 'spec_helper'

describe CrawlerStatus do
  context "running status" do
    it 'running_now' do
      CrawlerStatus.running_now?.should be_false
    end
    
    it 'not running_now' do
      FactoryGirl.create(:crawler_status)
      CrawlerStatus.running_now?.should
    end
  end

  context "start crawler" do
    it "crawler not started" do
      CrawlerStatus.start_crawler
      CrawlerStatus.running_now?.should
    end

    it "crawler already started" do
      CrawlerStatus.start_crawler
      CrawlerStatus.should_receive(:running_now?).once
      CrawlerStatus.start_crawler
    end
  end

  context "end crawler" do

    it "crawler not started" do
      CrawlerStatus.end_crawler.should be_false
    end


    it "crawler started" do
      CrawlerStatus.start_crawler
      CrawlerStatus.end_crawler.should
    end


    context "creation of data_update_stat" do
      it "creation on end_crawler even start" do
        DataUpdateStat.should_receive(:create)
        CrawlerStatus.end_crawler
      end
    end
  end
end