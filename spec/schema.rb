ActiveRecord::Schema.define do
  self.verbose = false
  create_table :data_update_stats, :force => true do |t|
    t.text :content
    t.timestamps
  end

  create_table :workit_product_prices, :force => true do |t|
    t.integer :workit_bpl_list_id
    t.integer :base_product_id
    t.integer :category_id
    t.string :icrt_code
    t.integer :site_id
    t.string :site_name
    t.string :url
    t.string :brand
    t.string :title
    t.float :price
    t.float :variation
    t.boolean :availability
    t.integer :delivery
    t.string :image_url
    t.datetime :modified_at
    t.float :delivery_price
    t.string :status, :default => 'waiting-processing'
    t.text :import_errors
    t.text :processing_errors

    t.timestamps
  end

  create_table :workit_product_families, :force => true do |t|
    t.integer :category_id
    t.integer :country_id
    t.timestamps
  end

  create_table :workit_bpl_list, :force => true do |t|
    t.integer :workit_product_family_id
    t.integer :category_id
    t.integer :base_product_id
    t.string :brand
    t.string :model
    t.string :name
    t.string :icrt
    t.timestamps
  end

  create_table "input_base_products", :force => true do |t|
    t.string   "brand",                      :limit => 765
    t.string   "model",                      :limit => 765
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "country",                    :limit => 765
    t.string   "encodex_id",                 :limit => 765
    t.integer  "provided_activation_status", :limit => 2
    t.integer  "category_id"
    t.integer  "base_product_id"
    t.integer  "first_activity_period",      :limit => 2
    t.string   "ean",                        :limit => 300
    t.datetime "deleted_at"
    t.datetime "first_date"
    t.string   "name"
    t.integer  "tested",                     :limit => 1
    t.decimal  "EncodexPrice"
    t.string   "ComparioId"
    t.string   "photo"
    t.string   "ProdNom"
    t.float    "ttr",                        :limit => 24
    t.string   "icrt"
    t.string   "ProductName"
    t.boolean  "best_buy"
    t.boolean  "best_of_the_test"
    t.boolean  "profitable_choice"
  end


  create_table "all_products", :force => true do |t|
    t.string   "name",             :limit => 1024,                :null => false
    t.string   "price"
    t.string   "url",              :limit => 1024
    t.integer  "site_id"
    t.integer  "site_category_id"
    t.string   "raw_price"
    t.boolean  "accessory"
    t.string   "raw_name",         :limit => 1024
    t.boolean  "in_stock"
    t.string   "unique_id"
    t.integer  "active",           :limit => 2,    :default => 1
    t.date     "last_found"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "country"
    t.integer  "trovaprezzi",                      :default => 0
    t.string   "lastrun"
  end


  create_table "all_product_prices", :force => true do |t|
    t.string   "unique_id"
    t.date     "price_date"
    t.float    "price",                         :limit => 24
    t.string   "price_currency"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "notification_rules_message_id"
    t.string   "indicator_type_used",           :limit => 1,  :default => "y"
    t.integer  "matched_with"
    t.string   "reason_rejected"
    t.integer  "data_update_stat_id"
  end

  create_table "base_product_countries", :force => true do |t|
    t.string  "base_product_name"
    t.integer "base_product_id"
    t.string  "base_product_unique_id"
    t.string  "country"
  end

  create_table "base_product_prices", :force => true do |t|
    t.string   "unique_id"
    t.date     "price_date"
    t.float    "min_price",      :limit => 24
    t.float    "max_price",      :limit => 24
    t.string   "country"
    t.string   "price_currency"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "base_product_synonymous", :force => true do |t|
    t.string  "country"
    t.string  "brand_synonym"
    t.string  "model_synonym"
    t.integer "base_product_id"
    t.integer "category_id"
    t.string  "brand_old"
    t.string  "model_old"
    t.integer "synonymou_id"
  end

  create_table "base_products", :force => true do |t|
    t.string   "brand"
    t.string   "model"
    t.integer  "category_id",                     :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "activation_status"
    t.string   "name"
    t.string   "unique_id"
    t.string   "lastrun"
    t.string   "modified_name"
    t.float    "ttr",               :limit => 24
    t.string   "icrt"
    t.boolean  "best_buy"
    t.boolean  "best_of_the_test"
    t.boolean  "profitable_choice"
  end

  create_table "basic_products_base_products", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "match_type"
    t.string   "product_unique_id"
    t.string   "base_product_unique_id"
    t.integer  "user_id"
    t.string   "user_name"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "products", :force => true do |t|
    t.string   "name",                          :limit => 1024,                   :null => false
    t.string   "price"
    t.string   "url",                           :limit => 1024
    t.integer  "site_id",                                                         :null => false
    t.integer  "site_category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "raw_price"
    t.boolean  "accessory"
    t.string   "raw_name",                      :limit => 1024
    t.boolean  "in_stock",                                      :default => true
    t.string   "unique_id"
    t.boolean  "active",                                        :default => true
    t.date     "last_found"
    t.string   "country"
    t.string   "lastrun"
    t.integer  "trovaprezzi",                                   :default => 0
    t.decimal  "currency"
    t.string   "Numerical_price",               :limit => 8
    t.integer  "notification_rules_message_id"
    t.string   "indicator_type_used",           :limit => 1
  end

  create_table "site_categories", :force => true do |t|
    t.integer  "category_id",                                            :null => false
    t.integer  "site_id",                                                :null => false
    t.string   "listing_url",          :limit => 2500
    t.string   "sub_category_name"
    t.text     "css_selector"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "status_crawler",                       :default => true
    t.datetime "lasted_at_status"
    t.boolean  "status_publish_to_ps"
  end

  create_table "crawler_statuses", :id => false, :force => true do |t|
    t.datetime "process_timestamp"
    t.string   "action"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status"
    t.boolean  "indicator_calculated"
    t.datetime "date_publish_ps"
    t.string   "status_publish_ps"
  end

  create_table "sites", :force => true do |t|
    t.string   "name"
    t.text     "css_selector"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "country"
    t.boolean  "disabled",       :default => false
    t.boolean  "null_urls",      :default => false
    t.string   "pretty_name"
    t.string   "source",         :default => "crawler"
    t.integer  "products_count", :default => 0
    t.string   "unique_id"
    t.string   "evaluated"
    t.boolean  "excluded"
  end
end